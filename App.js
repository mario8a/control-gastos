import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Alert,
  Pressable,
  Image,
  Modal,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
//Components
import { Header } from './src/components/Header';
import { NuevoPresupuesto } from './src/components/NuevoPresupuesto';
import { ControlPresupuesto } from './src/components/ControlPresupuesto';
import { FormularioGasto } from './src/components/FormularioGasto';
import { generarId } from './src/helpers';
import { ListadoGastos } from './src/components/ListadoGastos';
import { Filtro } from './src/components/Filtro';


const App = () => {

  const [isValidPresupuesto, setIsValidPresupuesto] = useState(false);
  const [presupuesto, setPresupuesto] = useState(0);
  const [gastos, setGastos] = useState([]);
  const [modal, setModal] = useState(false);
  const [gasto, setGasto] = useState({});
  const [filtro, setFiltro] = useState('');
  const [gastosFiltrados, setGastosFiltrados] = useState([]);

  useEffect(() => {
    const obtenerPresupuesto = async () => {
      try {
        // ?? -> Nullish, puede venir null el valor en caso de que venta asignale 0
        const presupuestoStorage = await AsyncStorage.getItem('planificador_presupuesto') ?? 0;

        if (presupuestoStorage > 0) {
          setPresupuesto(presupuestoStorage);
          setIsValidPresupuesto(true);
        }
      } catch (error) {
        console.log(error);
      }
    };
    obtenerPresupuesto();
  }, []);

  useEffect(() => {
    const obtenerGastos = async () => {
      try {
        const gastosStorage = await AsyncStorage.getItem('planificador_gastos');
        setGastos(gastosStorage ? JSON.parse(gastosStorage) : []);
      } catch (error) {
        console.log(error);
      }
    }
    obtenerGastos();
  }, []);

  useEffect(() => {
    const guardarGastosStorage = async () => {
      try {
       await AsyncStorage.setItem('planificador_gastos', JSON.stringify(gastos));
      } catch (error) {
        console.log(error);
      }
    };
    guardarGastosStorage();
  }, [gastos]);

  useEffect(() => {
    if (isValidPresupuesto) {
      const almacenarAS = async () => {
        try {
          await AsyncStorage.setItem('planificador_presupuesto', presupuesto);
        } catch (error) {
          console.log(error);
        }
      };
      almacenarAS();
    }
  }, [isValidPresupuesto]);

  const handleNuevoPresupuesto = (presu) => {
    if (Number(presu) > 0)  {
      setIsValidPresupuesto(true);
    } else {
      Alert.alert('Error', 'El presupuesto debe ser mayor a 0');
    }
  };

  const handleGasto = (gasto) => {

     if ([gasto.nombre, gasto.categoria, gasto.cantidad].includes('')) {
       //hay uno vacio
       Alert.alert(
         'Error',
          'Todos los campos son obligatorios'
       );
       return;
      }

      if (gasto.id) {
        const gastosActualizados = gastos.map(gastoState => gastoState.id === gasto.id ? gasto : gastoState);
        setGastos(gastosActualizados);
      } else {
        gasto.id = generarId();
        gasto.fecha = Date.now();
        setGastos([...gastos, gasto]);
      }

      setModal(!modal);

  };

  const eliminarGasto = (id) => {
    Alert.alert(
      '¿Deseas eliminar el gasto?',
      'Un gasto eliminado no se puede recuperar',
      [
        {text: 'No', style: 'cancel'},
        {text: 'Si', onPress: () => {
          const gastosActualizados = gastos.filter(gastoState => gastoState.id !== id);
          setGastos(gastosActualizados);
          setModal(!modal);
          setGasto({});
        }},
      ]
    );
  };

  const resetearApp = () => {
    Alert.alert('Deseas resetear la app?', 'Esta acción no se puede deshacer',
      [
        {text: 'No', style: 'cancel'},
        { text: 'Si, eliminar', onPress: async () => {
          try {
            await AsyncStorage.clear();
            setIsValidPresupuesto(false);
            setPresupuesto(0);
            setGastos([]);
          } catch (error) {
            console.log(error);
          }
        }},
      ]
    );
  };

  return (
    <View style={styles.contenedor}>
      <ScrollView>
        <View style={styles.header}>
          <Header />

          {isValidPresupuesto ? (
            <ControlPresupuesto
              presupuesto={presupuesto}
              gastos={gastos}
              resetearApp={resetearApp}
            />
          ) : (
            <NuevoPresupuesto
              presupuesto={presupuesto}
              setPresupuesto={setPresupuesto}
              handleNuevoPresupuesto={handleNuevoPresupuesto}
            />
          )}
        </View>

        {
          isValidPresupuesto && (
            <>
              <Filtro
                filtro={filtro}
                setFiltro={setFiltro}
                gastos={gastos}
                setGastosFiltrados={setGastosFiltrados}
              />
              <ListadoGastos
                gastos={gastos}
                setModal={setModal}
                setGasto={setGasto}
                filtro={filtro}
                gastosFiltrados={gastosFiltrados}
              />
            </>
          )
        }
      </ScrollView>
      {
        modal && (
          <Modal
            animationType='slide'
            visible={modal}
          >
            <FormularioGasto
              setModal={setModal}
              handleGasto={handleGasto}
              gasto={gasto}
              setGasto={setGasto}
              eliminarGasto={eliminarGasto}
            />
          </Modal>
        )
      }

      {isValidPresupuesto && (
        <Pressable
          style={styles.pressable}
          onPress={() => setModal(!modal)}
        >
          <Image
            style={styles.imagen}
            source={require('./src/img/nuevo-gasto.png')}
          />
        </Pressable>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    backgroundColor: '#F5F5F5',
    flex: 1,
  },
  header: {
    backgroundColor: '#3B82F6',
    minHeight: 400,
  },
  imagen: {
    width: 60,
    height: 60,
  },
  pressable: {
    position: 'absolute',
    bottom: 40,
    right: 30,
  }
});

export default App;
