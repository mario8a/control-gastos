import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Pressable,
} from 'react-native';
import { formatearCantidad } from '../helpers';
import globalStyles from './../styles/index';
import CircularProgress from 'react-native-circular-progress-indicator';

export const ControlPresupuesto = ({presupuesto, gastos, resetearApp}) => {

  const [disponible, setDisponible] = useState(0);
  const [gastado, setGastado] = useState(0);
  const [porcentaje, setPorcentaje] = useState(0);

  useEffect(() => {
    const totalGastado = gastos.reduce((total, gasto) => Number(gasto.cantidad) + total, 0);
    const totalDisponible = presupuesto - totalGastado;

    const nuevoPorcentaje = (
      ((presupuesto - totalDisponible) / presupuesto) * 100
    );
    setPorcentaje(nuevoPorcentaje);

    setGastado(totalGastado);
    setDisponible(totalDisponible);
  }, [gastos]);

  return (
    <View style={styles.contenedor}>
      <View style={styles.centrarGrafica}>
        <CircularProgress
          value={porcentaje}
          duration={1000}
          radius={120}
          valueSuffix={'%'}
          title="Gastado"
          inActiveStrokeColor='#f5f5f5'
          activeStrokeColor='#3b82f6'
        />
      </View>

      <View style={styles.contenedorTexto}>

        <Pressable
          onPress={resetearApp}
          style={styles.boton}
        >
          <Text style={styles.textoBtn} >Reload App</Text>
        </Pressable>

        <Text style={styles.valor}>
          <Text style={styles.label}>Presupuesto: </Text>
          ${formatearCantidad(presupuesto)}
        </Text>

        <Text style={styles.valor}>
          <Text style={styles.label}>Disponible: </Text>
          ${formatearCantidad(disponible)}
        </Text>

        <Text style={styles.valor}>
          <Text style={styles.label}>Gastado: </Text>
          ${formatearCantidad(gastado)}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    ...globalStyles.contenedor,
  },
  centrarGrafica: {
    alignItems: 'center',
  },
  contenedorTexto: {
    marginTop: 50,
  },
  valor: {
   fontSize: 20,
   textAlign: 'center',
   marginBottom: 10,
  },
  label: {
    fontWeight: '700',
    color: '#3B82F6',
  },
  boton: {
    backgroundColor: '#DB2777',
    padding: 10,
    marginBottom: 40,
    borderRadius: 5,
  },
  textoBtn: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: '700',
    textTransform: 'uppercase',
  },
});
