import React, {useState, useEffect} from 'react';
import {
  Text,
  SafeAreaView,
  View,
  TextInput,
  StyleSheet,
  Pressable,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import globalStyles from '../styles';

export const FormularioGasto = ({setModal, handleGasto, setGasto, gasto, eliminarGasto}) => {
  const [nombre, setNombre] = useState('');
  const [cantidad, setCantidad] = useState('');
  const [categoria, setCategoria] = useState('');
  const [id, setId] = useState('');
  const [fecha, setFecha] = useState('');

  useEffect(() => {
    if (gasto?.nombre) {
      setNombre(gasto.nombre);
      setCantidad(gasto.cantidad);
      setCategoria(gasto.categoria);
      setId(gasto.id);
      setFecha(gasto.fecha);
    }
  }, [gasto]);

  return (
    <SafeAreaView style={styles.contenedor}>
      <View style={styles.containerBtns}>
        <Pressable
          onPress={() => {
            setModal(false);
            setGasto({});
          }}
          style={styles.btnCancelar}>
          <Text style={styles.btnCancelarTxt}>Cancelar</Text>
        </Pressable>

        { !!id && (
          <Pressable
            onPress={() => eliminarGasto(id)}
            style={styles.btnCancelar}>
            <Text style={styles.btnCancelarTxt}>Eliminar</Text>
          </Pressable>
        ) }

      </View>

      <View style={styles.formulario}>
        <Text style={styles.titulo}> {gasto?.nombre ? 'Editar' : 'Nuevo gasto'}</Text>

        <View style={styles.campo}>
          <Text style={styles.label}>Nombre gasto:</Text>
          <TextInput
            style={styles.input}
            placeholder="Nombre del gasto. Ej. comida"
            value={nombre}
            onChangeText={setNombre}
          />
        </View>

        <View style={styles.campo}>
          <Text style={styles.label}>Cantidad gasto:</Text>
          <TextInput
            style={styles.input}
            placeholder="Cantidad del gasto. Ej. 3300"
            keyboardType="numeric"
            value={cantidad}
            onChangeText={setCantidad}
          />
        </View>

        <View style={styles.campo}>
          <Text style={styles.label}>Categoria gasto:</Text>
          <Picker
            style={styles.input}
            selectedValue={categoria}
            onValueChange={valor => setCategoria(valor)}>
            <Picker.Item label="-Seleccione-" value="" />
            <Picker.Item label="Ahorro" value="ahorro" />
            <Picker.Item label="Alimentación" value="alimentacion" />
            <Picker.Item label="Casa" value="casa" />
            <Picker.Item label="Gastos varios" value="gastos" />
            <Picker.Item label="Ocio" value="ocio" />
            <Picker.Item label="Salud" value="salud" />
            <Picker.Item label="Suscripciones" value="suscripciones" />
          </Picker>
        </View>
        <Pressable
          style={styles.submitBtn}
          onPress={() => handleGasto({nombre, cantidad, categoria, id, fecha})}>
          <Text style={styles.submitBtnTxt}> {gasto?.nombre ? 'Guardar' : 'Agregar gasto'}</Text>
        </Pressable>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    backgroundColor: '#1E40AF',
    flex: 1,
  },
  titulo: {
    textAlign: 'center',
    fontSize: 28,
    marginBottom: 30,
    color: '#64748B',
  },
  formulario: {
    ...globalStyles.contenedor,
  },
  campo: {
    marginVertical: 10,
  },
  label: {
    color: '#64748B',
    textTransform: 'uppercase',
    fontSize: 16,
    fontWeight: 'bold',
  },
  input: {
    backgroundColor: '#F5F5F5',
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
  },
  submitBtn: {
    backgroundColor: '#3B82F6',
    padding: 10,
    marginTop: 10,
    borderRadius: 10,
  },
  submitBtnTxt: {
    textAlign: 'center',
    color: '#FFF',
    fontWeight: '700',
    textTransform: 'uppercase',
  },
  btnCancelar: {
    backgroundColor: '#DB2777',
    padding: 10,
    marginTop: 30,
    marginHorizontal: 10,
    flex: 1,
  },
  btnCancelarTxt: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: '#FFF',
    textAlign: 'center',
  },
  containerBtns: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});
