export const formatearCantidad = (cantidad) => {
  return Number(cantidad).toLocaleString("en-US", {style:"currency", currency:"USD"});
};

export const generarId = () => {
  const random = Math.random().toString(36).substring(2, 11);
  const fecha = new Date().getTime().toString(36);

  return random + fecha;
};

export const formatoFecha = (fecha) => {
   const fechaNueva = new Date(fecha);
   const options = {
     year: 'numeric',
     month: 'long',
     day: '2-digit',
   };

   return fechaNueva.toLocaleDateString('es-ES', options);
};
